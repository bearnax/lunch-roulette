
lunchplace = {
    "name" : "",
    "price" : "", #$, $$, $$$, $$$$, $$$$$
    "cuisine" : "",
    "street-address" : "",
    "city" : "",
    "zip" : ""
}

restaurant_pool = {
    "r1" : {
        "name" : "El Ray del Taco",
        "price" : "$",
        "cuisine" : "Mexican",
        "street-address" : "5288 Buford Hwy NE",
        "city" : "Doraville",
        "zip" : "30346"
    },
    "r2" : {
        "name" : "Super H Mart",
        "price" : "$",
        "cuisine" : "Asian",
        "street-address" : "B, 6035 Peachtree Rd",
        "city" : "Doraville",
        "zip" : "30360"
    },
    "r3" : {
        "name" : "Atlanta Chinatown",
        "price" : "$$",
        "cuisine" : "Asian",
        "street-address" : "5383 New Peachtree Rd",
        "city" : "Atlanta",
        "zip" : "30341"
    },
    "r4" : {
        "name" : "Mellow Mushroom",
        "price" : "$$",
        "cuisine" : "Pizza",
        "street-address" : "5575 Chamblee Dunwoody Rd",
        "city" : "Atlanta",
        "zip" : "30338"
    }
}
