####Sequoia Capital invests $100m in Zoom.us
######You may have heard of some of Sequoia's previous small investments like Apple, Google, PayPal, and Stripe. They decided that in a the world of 'Houseparty,' 'Skype,' 'Facebook Live,' and pretty much every other company on the planet, that Zoom.us is the best value for their money.

> Check it out
https://zoom.us/


Categories

Finance / Acquisitions

[Atlassian acquires Trello for $425 million](https://techcrunch.com/2017/01/09/atlassian-acquires-trello/)

[Why is Trello worth $425 million?](https://www.wired.com/2017/01/trello-simple-app-worth-425-million-dollars/)

[Top Startup Investment Trends from 2016](https://medium.com/startup-grind/what-the-future-looks-like-top-trends-in-2016-startup-investing-7411c28e6174#.6dfuzq4n2)

Product Trends & News
##Facebook and Google own EVERYTHING
>The top 8 apps are all owned by either Facebook or Google. What does this mean?

[Uber Movement](https://movement.uber.com/cities)
>Uber is providing access to anonymized data from over 2 billion users to cities to improve urban planning around the world.

[Alexa had a Big Year at CES](https://www.cnet.com/news/whats-alexa-up-to-at-ces-heres-a-running-list-ces-2017/)
>So, here's and article with all the announcements.



Product Mgmt
[How to Ask Good Questions](https://jvns.ca/blog/good-questions/)
>Now with Comics!

Learn About Something Different

[The Moving Sofa Problem](https://www.math.ucdavis.edu/~romik/movingsofa/)
>The mathematician Leo Moser posed in 1966 the following curious mathematical problem: what is the shape of largest area in the plane that can be moved around a right-angled corner in a two-dimensional hallway of width 1
